;;;; package.lisp

#+ccl (rename-package 'misc 'misc '(m))

(defpackage #:usb-sp-lib
  (:use #:cl)
  #-ccl (:local-nicknames (#:cffi #:cffi)
                    (#:m #:misc)
                    (#:uiop #:uiop))
  (:export #:*in-buf*
           #:*port*
           #:*port-list*
           #:*port-status*
           #:usb-port-close
           #:usb-port-open
           #:usb-port-read
           #:usb-port-status
           #:usb-port-write))
