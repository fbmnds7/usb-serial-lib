
#|
(defpackage :cnc-host/usb-sp
  (:use :common-lisp :cffi))
|#

(in-package :usb-sp-lib)

(defvar it)

(cffi:define-foreign-library libserialport
    ;;(:darwin (:or "libserialport.0.dylib" "libserialport.dylib"))
    (:unix (:or "libserialport.so.0" "libserialport.so"))
  (t (:default "libserialport")))

(cffi:use-foreign-library libserialport)

(cffi:defcstruct (c-sp-port :class sp-port) 
    (name (:pointer :char))
  (description (:pointer :char))
  (sp-transport :int)
  (usb-bus :int)
  (usb-address :int)
  (usb-vid :int)
  (usb-pid :int)
  (usb-manufacturer (:pointer :char))
  (usb-product (:pointer :char))
  (usb-serial (:pointer :char))
  (bluetooth-address (:pointer :char))
  (fd :int))

(cffi:defctype sp-port-ptr (:pointer (:struct c-sp-port)))
(cffi:defctype sp-port-array-ptr (:pointer sp-port-ptr))

(cffi:defcenum sp-return
    (:sp-ok 0)
  (:sp-err-arg  -1)
  (:sp-err-fail -2)
  (:sp-err-mem  -3)
  (:sp-err-supp -4))

(cffi:defcenum sp-mode
    (:sp-mode-read       1)
  (:sp-mode-write      2)
  (:sp-mode-read-write 3))

(cffi:defcenum sp-parity
    (:sp-parity-invalid -1)
  (:sp-parity-none     0)
  (:sp-parity-odd      1)
  (:sp-parity-even     2)
  (:sp-parity-mark     3)
  (:sp-parity-space    4))

(cffi:defcenum sp-flowcontrol
    (:sp-flowcontrol-none    0)
  (:sp-flowcontrol-xonxoff 1)
  (:sp-flowcontrol-rtscts  2)
  (:sp-flowcontrol-dtrdsr  3))

(defconstant +sp-mode-read-write+
  #.(cffi:foreign-enum-value 'sp-mode :sp-mode-read-write))
(defconstant +sp-parity-none+
  #.(cffi:foreign-enum-value 'sp-parity :sp-parity-none))
(defconstant +sp-flowcontrol-none+
  #.(cffi:foreign-enum-value 'sp-flowcontrol :sp-flowcontrol-none))

(defconstant +port-conf-flags+       +sp-mode-read-write+)
(defconstant +port-conf-baudrate+    115200)
(defconstant +port-conf-bits+        8)
(defconstant +port-conf-parity+      +sp-parity-none+)
(defconstant +port-conf-stopbits+    1)
(defconstant +port-conf-flowcontrol+ +sp-flowcontrol-none+)

(cffi:defcfun "sp_list_ports" sp-return (port-list :pointer))

(cffi:defcfun "sp_get_port_name" :string (port :pointer))

(cffi:defcfun "sp_copy_port" sp-return (port-src :pointer) (port-dest :pointer))

(cffi:defcfun "sp_free_port_list" sp-return (port-list :pointer))

(cffi:defcfun "sp_open" sp-return (port :pointer) (port-conf-flags :int))

(cffi:defcfun "sp_set_baudrate" :void (port :pointer) (port-conf-baudrate :int))

(cffi:defcfun "sp_set_bits" :void (port :pointer) (port-conf-bits :int))

(cffi:defcfun "sp_set_parity" :void (port :pointer) (port-conf-parity :int))

(cffi:defcfun "sp_set_stopbits" :void (port :pointer) (port-conf-stopbits :int))

(cffi:defcfun "sp_set_flowcontrol" :void (port :pointer) (port-conf-flowcontrol :int))

(cffi:defcfun "sp_blocking_write" :int
  (port :pointer)
  (out_buf :string)
  (out_buf_bytes :int)
  (timeout_ms :int))

(cffi:defcfun "sp_blocking_read" :int
  (port :pointer)
  (in_buf :string)
  (in_buf_bytes :int)
  (timeout_ms :int))

(cffi:defcfun "sp_close" sp-return (port :pointer))

(cffi:defcfun "sp_free_port" :void (port :pointer))

(cffi:defcfun "sp_get_port_by_name" sp-return (name :string) (port :pointer))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defconstant +in-buf-size+ 1000)
(defconstant +out-buf-size+ 1000)
(defconstant +timeout-ms+ 1000)

(defvar +sp-ports+ (list "/dev/ttyACM0" "/dev/ttyACM1"
                         "/dev/ttyUSB0" "/dev/ttyUSB1"))

(defparameter *in-buf* (cffi:foreign-string-alloc (make-string +in-buf-size+)))
(defparameter *out-buf* (cffi:foreign-string-alloc (make-string +out-buf-size+)))

(defparameter *port-list* (cffi:foreign-alloc :pointer))
(defparameter *port* (cffi:foreign-alloc :pointer))
(defparameter *port-status* :close) ; :open :close :error

(defun %port-tty ()
  (some #'identity (remove-if-not #'uiop:file-exists-p +sp-ports+)))

(defparameter *port-tty* (or (%port-tty) "/dev/ttyACM0"))

(defun usb-port-close ()
  (let ((res (sp-close *port*)))
    ;;(sp-free-port *port*)
    ;;(sp-free-port-list (mem-ref *port-list* :pointer))
    (if (eql :usb-sp-ok res)
        (progn
          (setf *port-status* :close)
          nil)
        res)))

(defun usb-port-open (&optional (port-tty nil) (forced nil))
  (when (and (not forced)
             (or (null port-tty)
                 (equal port-tty *port-tty*))
             (eql :open *port-status*))
    (return-from usb-port-open))
  (if port-tty
      (setf *port-tty* port-tty)
      (setf *port-tty* (or (%port-tty) "/dev/ttyACM0")))
  (print *port-tty*)
  ;;(setf *port-list* (cffi:foreign-alloc :pointer))
  ;; struct sp_port **port_list = NULL;
  ;;(setf (cffi:mem-ref *port-list* :pointer) (cffi:null-pointer))
  (format t "Reinit *port-list* done")
  (format t "(sp-list-ports *port-list*) : ~A~%" (sp-list-ports *port-list*))
  (format t "(sp-get-port-by-name name *port-list*) : ~A~%"
          (sp-get-port-by-name *port-tty* *port-list*))
  (setf *port* (cffi:mem-ref *port-list* :pointer))
  (let ((res (sp-open *port* +port-conf-flags+)))
    (setf *port-status* (if (eql :sp-ok res) :open :error))
    (format t "(sp-open *port* +port-conf-flags+) : ~A~%" res)
    (if (eql :open *port-status*)
        (progn
          (sp-set-baudrate    *port* +port-conf-baudrate+)
          (sp-set-bits        *port* +port-conf-bits+)
          (sp-set-parity      *port* +port-conf-parity+)
          (sp-set-stopbits    *port* +port-conf-stopbits+)
          (sp-set-flowcontrol *port* +port-conf-flowcontrol+)
          nil)
        (values *port-status* res))))

(defun usb-port-write (s)
  (if (eql :open *port-status*)
      (let ((s-len (length s))
            (foreign-char-size (cffi:foreign-type-size :char)))
        (cffi:lisp-string-to-foreign (make-string (+ 2 s-len))
                                     *out-buf* +out-buf-size+)
        (dotimes (i s-len)
          (setf (cffi:mem-ref *out-buf* :char (* i foreign-char-size))
                (char-int (aref s i))))
        (setf (cffi:mem-ref *out-buf* :char (* s-len foreign-char-size)) 10)
        (setf (cffi:mem-ref *out-buf* :char
                            (* (1+ s-len) (cffi:foreign-type-size :char)))
              0)
        (let ((res (sp-blocking-write *port*
                                      *out-buf* +out-buf-size+ +timeout-ms+)))
          (setf *port-status* (if (> res 0) :open :error))
          (if (eql :open *port-status*) nil (values *port-status* res))))
      (values *port-status* :usb-io-err)))

(defun usb-port-read ()
  (if (eql :open *port-status*)
      (progn
        (cffi:lisp-string-to-foreign
         (make-string +in-buf-size+) *in-buf* +in-buf-size+)
        (let ((res (sp-blocking-read *port*
                                     *in-buf* +in-buf-size+ +timeout-ms+)))
          (if (> res 0)
              (values (cffi:foreign-string-to-lisp *in-buf*) nil)
              (values nil res))))
      (values nil :usb-io-err)))




