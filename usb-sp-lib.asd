;;;; usb-serial-lib.asd

(asdf:defsystem #:usb-sp-lib
  :description "Describe usb-serial-lib here"
  :author "Your Name <your.name@example.com>"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :depends-on (#:cffi #:uiop #:misc)
  :components ((:file "package")
               (:file "usb-serial-lib")))
